package atk.twitterStream;

import AEPgb.AEPgb;
import AEPgb.PgbSettings;
import atk.twitterStream.model.Tweet;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;


public class Main {



    public static final AEPgb gb = new AEPgb();
    public static Keys lastClick = Keys.none;
    public static volatile AtomicInteger noClickTime = new AtomicInteger(0);

    public static void emuClick(Keys key)  {
        if(lastClick == key)
            return;

        gb.joy.b_a = false;
        gb.joy.b_b = false;
        gb.joy.b_select = false;
        gb.joy.b_start = false;
        gb.joy.c_down = false;
        gb.joy.c_up = false;
        gb.joy.c_left = false;
        gb.joy.c_right = false;

        if(key == Keys.a) {
            gb.joy.b_a = true;
        } else if(key == Keys.b) {
            gb.joy.b_b = true;
        } else if(key == Keys.start) {
            gb.joy.b_start = true;
        } else if(key == Keys.select) {
            gb.joy.b_select = true;
        } else if(key == Keys.up) {
            gb.joy.c_up = true;
        } else if(key == Keys.left) {
            gb.joy.c_left = true;
        } else if(key == Keys.right) {
            gb.joy.c_right = true;
        } else if(key == Keys.down) {
            gb.joy.c_down = true;
        }

        lastClick = key;
    }

    public static void whenTweeted(String nick, String text) {

        text = text.toLowerCase();
        if(Config.HASHTAG != null) {
            text = text.replace(Config.HASHTAG.toLowerCase(), "");
        }

        text = text.replace("\n", "");
        text = text.replaceAll("[^A-Za-z0-9#!? ]", "?");
        text = text.trim();

        nick = nick.replace("\n", "");
        nick = nick.replaceAll("[^A-Za-z0-9#!? ]", "?");
        nick = nick.trim();

        Keys keyToBeClicked = Keys.none;

        for(Keys key : Keys.values()) {
            if(text.startsWith(key.name())) {
                keyToBeClicked = key;
            }
        }

        text = text.replaceFirst(keyToBeClicked.name(), "");

        System.out.println("[" + keyToBeClicked.name() + "] " + nick + ": " + text);
        emuClick(keyToBeClicked);
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        TweetObserver tweetObserver = new TweetObserver(Config.TWITTER_API_KEY, Config.TWITTER_API_SECRET, Config.TWITTER_API_TOKEN);
        tweetObserver.observe(new TweetObserver.OnTweetListener() {
            @Override
            public void onTweet(Tweet tweet, String inputLine) {
//                System.out.println(tweet);
                if(tweet != null && tweet.text != null) {
                    whenTweeted(tweet.user.name, tweet.text);
                } else {
//                    System.out.println(inputLine);
                }
            }
        }, Config.HASHTAG);

        gb.setCartSource(Config.ROM_FILENAME, Config.ROM_DIRECTORY);
        gb.setSystem(PgbSettings.SYS_GB);
        gb.go();

        Thread noClickWatcher = new Thread() {
            @Override
            public void run() {
                while(true) {
                    noClickTime.incrementAndGet();
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        noClickWatcher.start();

        TextAreaOutputStreamTest.createAndShowGui();

        while(true) {
            int time = noClickTime.get();
            if(time > 3) {
                emuClick(Keys.none);
                noClickTime.set(0);
            }
            Thread.yield();
        }
    }
}
