package atk.twitterStream.model;


public class Tweet {
    public String created_at;
    public long id;
    public String id_str;
    public String text;
    public String user_id_str;
    public long user_id;
    public User user;

    @Override
    public String toString() {
        return "Tweet{" +
                "text='" + text + '\'' +
                '}';
    }
}
