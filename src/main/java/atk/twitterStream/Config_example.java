package atk.twitterStream;

import org.scribe.model.Token;

public class Config_example {
    public static final String HASHTAG = "";
    public static final String ROM_FILENAME = "";
    public static final String ROM_DIRECTORY = "";

    public static final String TWITTER_API_KEY = "";
    public static final String TWITTER_API_SECRET = "";
    public static final Token TWITTER_API_TOKEN = new Token("", "");
}
