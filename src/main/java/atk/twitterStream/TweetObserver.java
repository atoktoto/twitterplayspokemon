package atk.twitterStream;

import atk.twitterStream.model.Tweet;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.*;

public class TweetObserver {

    final Gson gson = new Gson();
    final OAuthService service;
    final Token accessToken;

    final BlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(200);
    final ThreadPoolExecutor threadPool = new ThreadPoolExecutor(1, 200, 0L, TimeUnit.MILLISECONDS, queue);

    public TweetObserver(String twitterApiKey, String twitterApiSecret, Token twitterApiToken) {
        service = new ServiceBuilder()
                .provider(TwitterApi.SSL.class)
                .apiKey(twitterApiKey)
                .apiSecret(twitterApiSecret)
                .build();

        accessToken = twitterApiToken;
    }

    public void observe(final OnTweetListener listener, final String hashtag) {

        Future future = threadPool.submit(new Runnable() {
            @Override
            public void run() {
                try {

                    OAuthRequest request;

                    if(hashtag == null || hashtag.length() == 0) {
                        request = new OAuthRequest(Verb.GET, "https://stream.twitter.com/1.1/statuses/sample.json");
                    } else {
                        request = new OAuthRequest(Verb.POST, "https://stream.twitter.com/1.1/statuses/filter.json");
                        request.addBodyParameter("track", hashtag);
                    }

                    service.signRequest(accessToken, request); // the access token from step 4

                    Response response = request.send();

                    InputStream is = response.getStream();
                    BufferedReader in = new BufferedReader(new InputStreamReader(is));
                    String inputLine;

                    while ((inputLine = in.readLine()) != null) {
                        try {
                            Tweet tweet = gson.fromJson(inputLine, Tweet.class);
                            listener.onTweet(tweet, inputLine);
                        } catch (JsonSyntaxException e) {
                            System.out.println("exception = " + e);
                            System.out.println(inputLine);
                        }

                    }

                    System.out.println(inputLine);
                    in.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    public void shutdown() {
        threadPool.shutdown();
        try {
            threadPool.awaitTermination(1000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {}
    }

    public interface OnTweetListener {
        void onTweet(Tweet tweet, String inputLine);
    }
}
